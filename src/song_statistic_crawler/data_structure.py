from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import declarative_base

# declarative base class
Base = declarative_base()


class SongData(Base):
    __tablename__ = 'song_data_test'
    id = Column(Integer, primary_key=True)
    song = Column(String(length=255))
    artists = Column(String(length=65535))
    platform = Column(String(length=255))
    link = Column(String(length=255))
    title = Column(String(length=255))
    account = Column(String(length=255))
    n_plays = Column(Integer)
    duration = Column(Integer)
