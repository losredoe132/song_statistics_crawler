import numpy as np
import pandas as pd
import seaborn as sns

path = "../../../data.csv"
df = pd.read_csv(path)

# filter out
filter_1 = df["song"] == "Highway House Mix"


df = df[filter_1]

df["log_n_plays"] = np.log2(df["n_plays"])

g = sns.lineplot(x="time", y="n_plays", data=df)
