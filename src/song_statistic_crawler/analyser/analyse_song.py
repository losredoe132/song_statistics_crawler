
import configparser

import pandas as pd
from sqlalchemy import create_engine

path_config_db: str = "config/config_db.ini"

config_version = "reader"

config = configparser.ConfigParser()
config.read(path_config_db)
db_user = config[config_version]["user"]
db_password = config[config_version]["password"]
db_server = config[config_version]["server"]
db_database = config[config_version]["database"]

engine = create_engine(
    f"mariadb+mariadbconnector://{db_user}:{db_password}@{db_server}/{db_database}")

sql_query = "SELECT * FROM song_data_test"

df = pd.read_sql(sql_query, con=engine)


def get_plays(df, song: str = "", platform: str = "", artists: str = ""):
    """
    Description: Wauu *___*
    """
    song_module = df["song"].str.contains(song, case=False)
    platform_module = df["platform"].str.contains(platform, case=False)
    artists_module = df["artists"].str.contains(artists, case=False)

    if len(song) != 0 and len(platform) != 0 and len(artists) != 0:  # 111
        return df.loc[(song_module & platform_module & artists_module)]

    if len(song) != 0 and len(platform) != 0 and len(artists) == 0:  # 110
        return df.loc[(song_module & platform_module)]

    if len(song) != 0 and len(platform) == 0 and len(artists) != 0:  # 101
        return df.loc[(song_module & artists_module)]

    if len(song) == 0 and len(platform) != 0 and len(artists) != 0:  # 011
        return df.loc[(platform_module & artists_module)]

    if len(song) != 0 and len(platform) == 0 and len(artists) == 0:  # 100
        return df.loc[(song_module)]

    if len(song) == 0 and len(platform) != 0 and len(artists) == 0:  # 010
        return df.loc[(platform_module)]

    if len(song) == 0 and len(platform) == 0 and len(artists) != 0:  # 001
        return df.loc[(artists_module)]

    else:
        return df
