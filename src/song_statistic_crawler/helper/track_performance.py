import time

import numpy as np

from song_statistic_crawler.crawler.soundcloud.song_scraper import \
    SoundcloudTrackScraper

delta_ts = []
for i in range(10):
    print(i)
    t_0 = time.time()
    url = 'https://soundcloud.com/hate_music/premiere-rotor-militia-impact-mch002'
    print(SoundcloudTrackScraper(url).parse())
    delta_ts.append(time.time()-t_0)

print(f"{i} runs in {np.mean(delta_ts):.2f} +/- {np.std(delta_ts):.2f} s")
