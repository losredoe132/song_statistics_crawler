import configparser
import json
import logging
import os
import platform
import time

from sqlalchemy import create_engine, orm

from song_statistic_crawler.scraper.browser import Browser
from song_statistic_crawler.scraper.scraper_soundcloud import \
    SoundcloudSongScraper
from song_statistic_crawler.scraper.scraper_spotify import SpotifySongScraper
from song_statistic_crawler.scraper.scraper_youtube import YoutubeSongScraper


class Runner():
    def __init__(self):
        self.use_disp = False
        self.counter = 0
        self.n_urls = 0

        self.n_tries_allowed = 4
        self.period_duration_min = 60*60  # minimum wait between runs in seconds

        path_config_songs: str = "config/new_config.json"

        with open(path_config_songs) as fh:
            self.config_songs = json.load(fh)

    def upload_to_db(self, song_data_list):
        path_config_db: str = "config/config_db.ini"

        config = configparser.ConfigParser()
        config.read(path_config_db)
        config_version = "scraper"
        db_user = config[config_version]["user"]
        db_password = config[config_version]["password"]
        db_server = config[config_version]["server"]
        db_database = config[config_version]["database"]

        engine = create_engine(
            f"mariadb+mariadbconnector://{db_user}:{db_password}@{db_server}/{db_database}")

        DBSession = orm.sessionmaker()
        DBSession.configure(bind=engine)
        self.db_session = DBSession()

        # fetch to DB
        for song_data in song_data_list:
            self.db_session.add(song_data)
        try:
            self.db_session.commit()
            logging.info(f"succesfully commited {len(song_data_list)} entries in mariadb")
        except Exception as e:
            logging.warning(e)

    def run_spotify(self):
        self.counter = 1

        logging.info("--- RUN Spotify Scraper---")
        spotify_urls = []
        unique_spotify_urls = []
        song_names = []
        self.browser_with_proxy = Browser(use_browsermobproxy=True)  # with proxy
        logging.info("setup finished")

        # get unique spotify links and song names
        for item in self.config_songs:
            song_names.append(item["song"])
            for pf, pf_urls in item["urls"].items():
                if pf == "spotify":
                    spotify_urls.append(list(pf_urls.values())[0])

        unique_spotify_urls = list(set(spotify_urls))

        try:
            song_data_list = SpotifySongScraper(self, song_names, unique_spotify_urls).scrape()
        except Exception as e:
            logging.error(e)

        self.upload_to_db(song_data_list)
        self.browser_with_proxy.close_connections()

    def run_youtubesoundcloud(self):
        # -- RUN youtube and soundcloud --
        self.n_urls = self.dry_run()
        logging.info("--- RUN Youtube&Soundcloud Scraper---")
        self.browser = Browser(use_browsermobproxy=False)
        # iterate over platform
        for item in self.config_songs:
            for pf, pf_urls in item["urls"].items():
                self.pf = pf
                self.song = item["song"]
                self.artists = item["artists"]
                for account, url in pf_urls.items():
                    self.account = account

                    if pf == "soundcloud":
                        song_scraper = SoundcloudSongScraper(self)
                    if pf == "youtube":
                        song_scraper = YoutubeSongScraper(self)
                    if pf == "spotify":
                        # TODO remove jumping over spotify
                        logging.info("jump over spotify")
                        continue

                    # Scrape
                    try:
                        song_data_list = song_scraper.scrape(url)
                    except Exception as e:
                        logging.error(e)
                        continue

                    self.upload_to_db(song_data_list)
                    self.counter += 1

        self.browser.close_connections()

    def run(self):
        logging.info("--- START Runner ---")
        self.run_spotify()
        self.run_youtubesoundcloud()
        logging.info("--- EXIT Runner ---")

    def dry_run(self) -> int:
        counter = 0
        for item in self.config_songs:
            for pf, pf_urls in item["urls"].items():
                for account, url in pf_urls.items():
                    if pf == "spotify":
                        # TODO remove jumping over spotify
                        continue
                    counter += 1

        return counter

    def run_continuously(self):
        t_last = time.time()
        while True:
            if time.time() < t_last + self.period_duration_min:
                logging.info("### new run is triggered ###")
                self.run()


if __name__ == "__main__":
    # change working directory to be able to execute this file via cronjob
    if platform.release() == "5.10.92-v7l+":
        os.chdir("/home/pi/song_statistics_crawler")

    logging.basicConfig(filename='logs/runner.log',
                        format="%(asctime)s:%(levelname)s:%(message)s",
                        level=logging.INFO)

    logging.info(f"working directory: {os.getcwd()}")
    runner = Runner()
    runner.run()
