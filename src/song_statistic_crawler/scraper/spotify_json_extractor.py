import json
import logging
import re

# with open('spotify_data1.json') as f:
#    data = json.load(f)


class SpotifyJsonExtractor():
    def __init__(self, har, url):
        self.har = har
        self.url = url
        self.datas = []

    def analyze(self):
        data = self.har
        

        for i in range(len(data["log"]["entries"])):
            # check if text key exists
            if "text" in data["log"]["entries"][i]["response"]["content"].keys():
                subdata = data["log"]["entries"][i]["response"]["content"]["text"]
                if "{\"data\":{\"album\":{\"playability\":{\"playable\":true}" in subdata:
                    # load subdata as dict
                    subdata_dict = json.loads(subdata)

                    # iterate over track items
                    for track_item in subdata_dict['data']['album']['tracks']['items']:
                        song = track_item["track"]["name"]
                        #artist = track_item['track']['artists']['artists']['items'][0]["profile"]["name"]

                        n_plays = int(track_item['track']['playcount'])

                        if len(song) < 1:
                            logging.error(f"Song name could not be accessed")
                            continue

                        self.datas.append({"song": song,
                                      "artists": None,
                                      "n_plays": n_plays,
                                      "platform": "spotify",
                                      "url": self.url})

        return self.datas
