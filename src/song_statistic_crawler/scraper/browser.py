import logging
import platform
import signal
from contextlib import suppress

import psutil
from browsermobproxy import Server
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


class Browser():
    def __init__(self, use_browsermobproxy=False):
        self.use_browsermobproxy = use_browsermobproxy
        if use_browsermobproxy:
            # start proxy server
            browsermob_path = "/home/pi/browsermob-proxy-2.1.4/bin/browsermob-proxy"
            logging.debug("initialize proxy...")
            self.server = Server(browsermob_path)
            self.server.start()
            self.proxy = self.server.create_proxy()

        opts = webdriver.ChromeOptions()

        # TODO clean up input args for ChromeOptions
        if use_browsermobproxy:
            opts.add_argument("disable-extensions")
            opts.add_argument(
                '--proxy-server={host}:{port}'.format(host='localhost', port=self.proxy.port))
        opts.add_argument('ignore-certificate-errors')
        opts.add_argument("disable-gpu")
        opts.add_argument("no-sandbox")  # linux only
        opts.add_argument("headless")
        # opts.add_argument("--disable-webgl")
        # opts.add_argument("--disable-xss-auditor")
        # opts.add_argument("--disable-web-security")

        try:
            ser = Service("/usr/lib/chromium-browser/chromedriver")
            self.driver = webdriver.Chrome(service=ser, options=opts)
        except Exception as e:
            logging.warning(e)
            ser = Service(ChromeDriverManager().install())
            self.driver = webdriver.Chrome(service=ser, options=opts)

        if platform.release() == "5.10.92-v7l+":
            # decide depending on os if display emulation is needed
            logging.debug("setting up browser...")
            self.use_disp = True
            self.disp = Display(visible=False, size=(800, 600), backend="xvfb")
            self.disp.start()

    def close_connections(self):
        self.driver.close()
        if self.use_disp:
            self.disp.stop()
        if self.use_browsermobproxy:
            self.proxy.close()
            bmp_daemon = self.server
            if bmp_daemon is not None and bmp_daemon.process is not None:
                childs_process = None
                try:
                    cmd_process = psutil.Process(bmp_daemon.process.pid)
                    childs_process = cmd_process.children(recursive=True)
                    childs_process = [*childs_process, cmd_process]

                    bmp_daemon.stop()

                finally:
                    for child in childs_process:
                        # we can't accidentally kill newly created process
                        # we can kill only the process we have cached earlier
                        # if process was already finished we will get NoSuchProcess
                        # that we're just suppressing
                        with suppress(psutil.NoSuchProcess):
                            try:
                                child.send_signal(signal.SIGTERM)
                            except Exception as e:
                                logging.debug(e)

        logging.info("closed all connections")
