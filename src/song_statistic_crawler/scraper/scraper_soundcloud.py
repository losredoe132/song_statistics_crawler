import json
import logging
import time

from selenium.webdriver.common.by import By

from song_statistic_crawler.data_structure import SongData
from song_statistic_crawler.scraper.scraper import SongScraper


class SoundcloudSongScraper(SongScraper):
    def __init__(self, runner):
        self.pf="soundcloud"
        super().__init__(runner=runner)

    def be_patient(self):
        time.sleep(2)
