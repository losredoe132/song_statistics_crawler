import time

from song_statistic_crawler.scraper.scraper import SongScraper


class YoutubeSongScraper(SongScraper):
    def __init__(self, runner):
        self.pf = "youtube"
        self.agreement_button_x_path = '''/html/body/ytd-app/ytd-consent-bump-v2-lightbox/tp-yt-paper-
        dialog/div[4]/div[2]/div[5]/div[2]/ytd-button-renderer[2]/a/tp-yt-paper-button'''
        super().__init__(runner=runner)

    def be_patient(self):
        # wait until page is fully loaded
        time.sleep(3)
