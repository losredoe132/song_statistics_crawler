import json
import logging
import time

from selenium.webdriver.common.by import By

from song_statistic_crawler.data_structure import SongData


class SongScraper():
    def __init__(self, runner):
        self.t_0 = time.time()
        self.runner = runner
        self.browser = runner.browser
        self.n_tries_allowed = 3
        with open("config/config_x_path.json", "r") as fh:
            self.x_path_dict = json.load(fh)[self.pf]

    def integerize_string(self, string: str):
        return int("".join(char for char in string if char.isdigit()))

    def be_patient(self):
        '''customizable be patient method to realize different
        functionalities between get and parse'''
        pass

    def scrape(self, url):
        self.url = url
        attributes = ["title", "account", "n_plays"]
        flags_valid_data = {key: False for key in attributes}
        try_counter = 0
        success = False

        while not all(flags_valid_data.values()) and try_counter < self.n_tries_allowed:
            self.browser.driver.get(url)

            # custom be patient
            self.be_patient()

            for attribute in attributes:
                # iterate over spaths if more than one is given
                x_paths = self.x_path_dict[attribute]
                for x_path in x_paths:
                    logging.debug(f"scrape {url} {try_counter} try ")
                    try:
                        result = self.browser.driver.find_element(By.XPATH, x_path).text
                    except Exception as e:
                        logging.debug(e)
                        result = ""

                    if attribute == "title" and result != "":
                        flags_valid_data[attribute] = True
                        self.title = str(result)
                        break

                    if attribute == "account" and result != "":
                        flags_valid_data[attribute] = True
                        self.account = str(result)
                        break

                    if attribute == "n_plays" and result != "":
                        flags_valid_data[attribute] = True
                        self.n_plays = self.integerize_string(result)
                        break

            try_counter += 1

        song_data = None
        self.delta_t = int(time.time()-self.t_0)

        if all(flags_valid_data.values()):
            success = True
            song_data = SongData(song=str(self.runner.song),
                                 artists=str(self.runner.artists),
                                 platform=str(self.runner.pf),
                                 link=str(self.url),
                                 title=str(self.title),
                                 account=str(self.account),
                                 n_plays=int(self.n_plays),
                                 duration=self.delta_t)

        success_msg = {True: "successfully", False: "failed"}[success]

        logging.info(
            f"scrape {self.runner.counter:3}/{self.runner.n_urls} {success_msg} on {self.runner.pf} after {try_counter} try in {self.delta_t:.2f}s with url:{url}")
        return [song_data]
