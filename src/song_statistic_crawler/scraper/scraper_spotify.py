import logging
import time

from song_statistic_crawler.data_structure import SongData
from song_statistic_crawler.scraper.spotify_json_extractor import \
    SpotifyJsonExtractor


class SpotifySongScraper():
    def __init__(self, runner, song_names, unique_spotify_urls):
        self.t_0 = time.time()
        self.unique_spotify_urls = unique_spotify_urls
        self.song_names = song_names
        self.runner = runner
        self.proxy = runner.browser_with_proxy.proxy
        self.driver = runner.browser_with_proxy.driver

    def get(self):

        n_urls = len(self.unique_spotify_urls)

        spotify_raw = []
        for i, url in enumerate(self.unique_spotify_urls):
            # define web traffic interceptor
            logging.debug("starting traffic interceptor...")
            self.proxy.new_har('req', options={'captureHeaders': False, 'captureContent': True})
            logging.debug("navigate to target url...")
            logging.info(f"get spotify url {i:3}/{n_urls:3}: {url}")
            self.driver.get(url)
            time.sleep(10)

            result = self.proxy.har  # returns a HAR
            spotify_raw.append(SpotifyJsonExtractor(result, url=url).analyze())
            # TODO SongData object

        logging.info("get spotify raw data finished.")
        return spotify_raw

    def match(self, spotify_raw):
        '''matches songnames to spotify raw and returns a list of SongData'''
        song_datas = []
        n_song_name = len(self.song_names)
        for i, song_name in enumerate(self.song_names):
            logging.info(f"matching spotify url {i:3}/{n_song_name:3}: {song_name}")
            flag_success = False

            for spotify_raw_source in spotify_raw:
                for candidate in spotify_raw_source:
                    if song_name.upper() in candidate['song'].upper():
                        song_datas.append(SongData(song=str(candidate["song"]),
                                                   artists=str(candidate["artists"]),
                                                   platform=str(candidate["platform"]),
                                                   link=str(candidate["url"]),
                                                   title=str(None),
                                                   account=str(None),
                                                   n_plays=int(candidate["n_plays"]),
                                                   duration=int(0)))
                        # jump over next occurences
                        flag_success = True
                        continue
            if flag_success:
                logging.info(f"found valid candiate: {song_name} matches {candidate['song']}")
            else:
                logging.error(f"found no valid candiate for {song_name}")

        return song_datas

    def scrape(self):
        return self.match(self.get())
