FROM python:3

ENV VIRTUAL_ENV=.venv
RUN python -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
COPY . .

COPY requirements.txt .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 5000

COPY src/song_statistic_crawler/runner.py .
CMD ["python", "./runner.py"]