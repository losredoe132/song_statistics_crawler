SELECT
  timestamp AS "time",
  n_plays AS "n_plays", 
  song AS "title",
  account AS "account"
FROM song_data
ORDER BY timestamp