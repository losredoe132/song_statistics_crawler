echo generate venv
python3 -m venv .venv

echo activate venv 
source .venv/bin/activate

echo update pip
pip install --upgrade pip

echo install requirements
pip install -r requirements.txt

