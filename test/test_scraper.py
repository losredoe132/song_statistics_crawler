from song_statistic_crawler.scraper.scraper_soundcloud import SoundcloudSongScraper
from song_statistic_crawler.scraper.scraper_youtube import YoutubeSongScraper
from song_statistic_crawler.scraper.scraper_spotify import SpotifySongScraper

from song_statistic_crawler.runner import Runner
from song_statistic_crawler.scraper.browser import Browser


class TestClass:
    def setup_method(self, method):
        self.runner = Runner()
        self.runner.song = "test"
        self.runner.artists = "artists"
        self.runner.pf = "platform"
        self.runner.browser_with_proxy = Browser(use_browsermobproxy=True)
        self.runner.browser = Browser(use_browsermobproxy=False)

    def teardown_method(self, method):
        self.runner.browser.close_connections()
        self.runner.browser_with_proxy.close_connections()

    def test_youtube(self):
        url = 'https://www.youtube.com/watch?v=u9X5SQCDnio'
        data = YoutubeSongScraper(self.runner).scrape(url)[0]
        assert data.song != ""
        assert data.artists != ""
        assert data.n_plays != ""

    def test_soundcloud(self):
        url = 'https://soundcloud.com/hate_music/premiere-rotor-militia-impact-mch002'
        data = SoundcloudSongScraper(self.runner).scrape(url)[0]
        assert data.song != ""
        assert data.artists != ""
        assert data.n_plays != ""

    '''
    def test_spotify(self):
        song = ["South Street"]
        url = ['https://open.spotify.com/album/111SFRR03loBX2sYn86zqR']
        data = SpotifySongScraper(self.runner, song, url).scrape()[0]
        assert data.song != ""
        assert data.artists != ""
        assert data.n_plays != ""
    '''