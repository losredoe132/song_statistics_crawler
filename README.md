# song_statistics_crawler

Scraping YouTube, Soundcloud etc to get plays, likes and comments 

## Conventions
Naming of branches: 
```[feat/bug]-[id]-{title with - as sepeator}```

## Getting started
Init the project:
```chmod +x ./init_project.sh```
```./init_project.sh```


## Add apckages to requirements.txt
```pip freeze > requirements.txt```

## Access to the database from remote
Access is only allowed for user ```reader```and command ```SELECT```but from arbitrary host and with a appropriate password. 
```mysql --host=localhost --user=myname --password=password mydb ```
